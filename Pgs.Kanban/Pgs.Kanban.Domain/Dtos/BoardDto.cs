﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pgs.Kanban.Domain.Dtos
{
    public class BoardDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ListDto> Lists { get; set; }

        public BoardDto()
        {
            Lists = new List<ListDto>();
        }
    }
}